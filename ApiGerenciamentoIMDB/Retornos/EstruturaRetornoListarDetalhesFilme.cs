﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiGerenciamentoIMDB.Data;

namespace ApiGerenciamentoIMDB.Retornos
{
    public class EstruturaRetornoListarDetalhesFilme : EstruturaRetorno
    {
        public Filme Filme { get; set; }
        public double MediaAvaliacao { get; set; }
    }
}
