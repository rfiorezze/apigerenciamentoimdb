﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGerenciamentoIMDB.Retornos
{
    public class EstruturaRetornoIncluirUsuario : EstruturaRetorno
    {
        public EstruturaRetornoIncluirUsuario() { }
        public int IdUsuario { get; set; }
    }
}
