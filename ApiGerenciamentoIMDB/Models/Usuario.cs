﻿namespace ApiGerenciamentoIMDB.Models
{
    public class Usuario
    {
        /// <summary>
        /// Nome do Usuario
        /// </summary>
        public string NomeUsuario { get; set; }

        /// <summary>
        /// Senha do Usuario
        /// </summary>
        public string Senha { get; set; }
    }
}
