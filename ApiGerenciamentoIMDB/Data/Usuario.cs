﻿using ApiGerenciamentoIMDB.Retornos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ApiGerenciamentoIMDB.Data
{
    public class Usuario
    {
        /// <summary>
        /// Identificador do Usuario
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdUsuario { get; set; }

        /// <summary>
        /// Nome do Usuario
        /// </summary>
        [Required, Column(TypeName = "varchar(50)")]
        public string NomeUsuario { get; set; }

        /// <summary>
        /// Senha do Usuario
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string Senha { get; set; }

        /// <summary>
        /// Indicador de usuario Administrador
        /// </summary>
        [Required]
        public bool Administrator { get; set; }

        /// <summary>
        /// Indicador de usuario ativo
        /// </summary>
        [Required]
        public bool Ativo { get; set; }

    }
}
