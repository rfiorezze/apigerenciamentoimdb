﻿using ApiGerenciamentoIMDB.Retornos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ApiGerenciamentoIMDB.Data
{
    public class Voto
    {
        /// <summary>
        /// Identificador do Voto
        /// </summary>
        [Key, Required, JsonIgnore, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdVoto { get; set; }

        [JsonIgnore]
        public Usuario Usuario { get; set; }

        /// <summary>
        /// Id do Usuario
        /// </summary>
        [Required]
        public int UsuarioID { get; set; }

        [JsonIgnore]
        public Filme Filme { get; set; }

        /// <summary>
        /// Identificador do Filme
        /// </summary>
        [Required]
        public int FilmeID { get; set; }

        /// <summary>
        /// Avaliação do Filme
        /// </summary>
        [Required]
        public int Avaliacao { get; set; }

    }
}
