﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ApiGerenciamentoIMDB.Retornos;

namespace ApiGerenciamentoIMDB.Data
{
    public class Filme
    {
        /// <summary>
        /// Identificador do Filme
        /// </summary>
        [Key, Required, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdFilme { get; set; }

        /// <summary>
        /// Nome do Filme
        /// </summary>
        [Required, Column(TypeName = "varchar(150)")]
        public string NomeFilme { get; set; }

        /// <summary>
        /// Diretor do Filme
        /// </summary>
        [Required, Column(TypeName = "varchar(50)")]
        public string Diretor { get; set; }

        /// <summary>
        /// Genero do Filme
        /// </summary>
        [Required, Column(TypeName = "varchar(20)")]
        public string Genero { get; set; }

        /// <summary>
        /// Atores do Filme
        /// </summary>
        [Required, Column(TypeName = "varchar(500)")]
        public string  Atores { get; set; }
    }
}
