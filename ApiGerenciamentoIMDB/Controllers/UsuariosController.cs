﻿using ApiGerenciamentoIMDB.Data;
using ApiGerenciamentoIMDB.Retornos;
using ApiGerenciamentoIMDB.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGerenciamentoIMDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly BaseContext _context;

        public UsuariosController(BaseContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Apagar Usuario
        /// </summary>
        /// <remarks>
        /// Apaga um usuário no banco de dados       
        /// </remarks>
        /// <returns>Este método retorna um indicador que indica se foi apagado</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response>  
        [HttpDelete, Authorize]
        public async Task<IActionResult> ApagarUsuario(int idUsuario)
        {
            EstruturaRetorno estruturaRetorno = new EstruturaRetorno();

            var usuario = await _context.Usuario.FindAsync(idUsuario);

            usuario.Ativo = false;
            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                estruturaRetorno.IndicadorRetorno = -1;
                estruturaRetorno.DescricaoRetorno = ex.Message;
                return NotFound(estruturaRetorno);
            }

            return Ok(estruturaRetorno);
        }


        /// <summary>
        /// Alterar Usuario
        /// </summary>
        /// <remarks>
        /// Este método realiza uma alteração no cadastro de usuário.
        /// Sample request:
        ///
        ///     PUT / Usuario
        ///     {
        ///        "nomeUsuario": "Renato",
        ///        "senha": "renato1234",
        ///        "ativo": true
        ///     }
        ///
        /// </remarks>
        /// <returns>Este método retorna o identiicador do usuário criado</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response>   
        [HttpPut("{idUsuario}"), Authorize]
        public ActionResult AlterarUsuario(int idUsuario, Usuario usuario)
        {
            EstruturaRetorno estruturaRetorno = new EstruturaRetorno();

            try
            {
                var existingUsuario = _context.Usuario.Where(u => u.IdUsuario == usuario.IdUsuario).FirstOrDefault<Usuario>();

                if (existingUsuario != null)
                {
                    existingUsuario.NomeUsuario = usuario.NomeUsuario;
                    existingUsuario.Senha = usuario.Senha;
                    existingUsuario.Ativo = usuario.Ativo;
                    existingUsuario.Administrator = usuario.Administrator;

                    _context.SaveChanges();
                }
                else
                {
                    estruturaRetorno.IndicadorRetorno = -1;
                    estruturaRetorno.DescricaoRetorno = "Usuário não existente";
                    return NotFound(estruturaRetorno);
                }
            }
            catch (DbUpdateException ex)
            {
                estruturaRetorno.IndicadorRetorno = -1;
                estruturaRetorno.DescricaoRetorno = ex.Message;
                return NotFound(estruturaRetorno);
            }

            return Ok(estruturaRetorno);

        }


        /// <summary>
        /// Incluir Usuario
        /// </summary>
        /// <remarks>
        /// Este método realiza um cadastro de usuário.
        /// Sample request:
        ///
        ///     POST / Usuario
        ///     {
        ///        "nomeUsuario": "Renato",
        ///        "senha": "renato1234",
        ///        "ativo": true
        ///     }
        ///
        /// </remarks>
        /// <returns>Este método retorna o identiicador do usuário criado</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response>   
        [HttpPost]
        public ActionResult IncluirUsuario(Usuario usuario)
        {
            usuario.Administrator = false;
            usuario.IdUsuario = _context.Usuario.Count() + 1;

            EstruturaRetornoIncluirUsuario estruturaRetorno = new EstruturaRetornoIncluirUsuario();
            _context.Usuario.Add(usuario);

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                estruturaRetorno.IndicadorRetorno = -1;
                estruturaRetorno.DescricaoRetorno = ex.Message;
                return NotFound(estruturaRetorno);
            }

            estruturaRetorno.IdUsuario = usuario.IdUsuario;
            return Ok(estruturaRetorno);
        }

    }
}
