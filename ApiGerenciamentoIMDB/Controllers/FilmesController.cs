﻿using ApiGerenciamentoIMDB.Data;
using ApiGerenciamentoIMDB.Retornos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ApiGerenciamentoIMDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmesController : ControllerBase
    {
        private readonly BaseContext _context;

        public FilmesController(BaseContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Listar Filmes
        /// </summary>
        /// <remarks>
        /// Este método Lista os filmes mais populares cadastrados
        /// - Opções de filtragem por: Nome, Diretor, Genero e atores
        /// - Este método permite paginação de dados
        /// </remarks>
        /// <returns>Este método retorna os filmes mais populares cadastrados</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpGet, Authorize]
        public ActionResult ListarFilmesPopulares(string Nome, string Diretor, string Genero, string Atores, int pagina, int quantidadeRegistros)
        {
            try
            {
                var filmes = _context.Filme.ToList();
                var votos = _context.Voto.ToList();

                var query =
                    from filme in filmes
                    join voto in votos on filme.IdFilme equals voto.FilmeID into filmeVoto
                    select new { Filme = filme, Votos = filmeVoto.Count() };


                var resultado = query.OrderByDescending(c => c.Votos).ThenBy(n => n.Filme.NomeFilme).ToList();

                //Aplicando Filtros
                if (!string.IsNullOrEmpty(Nome))
                    resultado = resultado.Where(p => p.Filme.NomeFilme.ToLower().Contains(Nome.ToLower())).ToList();

                if (!string.IsNullOrEmpty(Diretor))
                    resultado = resultado.Where(p => p.Filme.Diretor.ToLower().Contains(Diretor.ToLower())).ToList();

                if (!string.IsNullOrEmpty(Genero))
                    resultado = resultado.Where(p => p.Filme.Genero.ToLower().Contains(Genero.ToLower())).ToList();

                if (!string.IsNullOrEmpty(Atores))
                    resultado = resultado.Where(p => p.Filme.Atores.ToLower().Contains(Atores.ToLower())).ToList();

                //Se escolher paginar
                if (pagina > 0)
                {
                    int totalPaginas = (int)Math.Ceiling(_context.Usuario.Count() / Convert.ToDecimal(quantidadeRegistros));

                    resultado = resultado
                        .Skip(quantidadeRegistros * (pagina - 1))
                        .Take(quantidadeRegistros)
                        .ToList();
                }

                return Ok(resultado);
            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }

        /// <summary>
        /// Listar Filme
        /// </summary>
        /// <remarks>
        /// Este método Consulta todos os detalhes de um determinado filme cadastrado
        /// </remarks>
        /// <returns>Este método retorna todos os detalhes de um determinado filme</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpGet("{id}"), Authorize]
        public ActionResult ListarDetalhesFilme(int id)
        {
            try
            {
                var filme = _context.Filme.Find(id);

                var votacao = _context.Voto.ToList();

                var somaVotos = votacao.Where(v => v.FilmeID == id).Sum(s => s.Avaliacao);

                var qtdVotos = votacao.Where(v => v.FilmeID == id).Count();

                double mediaVotos = somaVotos / qtdVotos;

                EstruturaRetornoListarDetalhesFilme estruturaRetorno = new EstruturaRetornoListarDetalhesFilme();

                estruturaRetorno.Filme = filme;
                estruturaRetorno.MediaAvaliacao = mediaVotos;

                return Ok(estruturaRetorno);

            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }

        /// <summary>
        /// Incluir Filme
        /// </summary>
        /// <remarks>
        /// Este método cadastra um filme no banco de dados.
        /// Sample request:
        ///
        ///     POST / Filme
        ///     {
        ///       "nomeFilme": "string",
        ///       "diretor": "string",
        ///       "genero": "string",
        ///       "atores": "string"
        ///     }
        ///
        /// </remarks>
        /// <returns>Este método retorna o identiicador do filme que foi criado</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpPost("{idUsuario}"), Authorize]
        public ActionResult IncluirFilme(int idUsuario, Filme filme)
        {
            var usuario = _context.Usuario.Find(idUsuario);

            //Valida se o usuário existe
            if (usuario == null)
                return NotFound("O Usuario com o id informado não foi encontrado!");

            //Valida se o usuário é administrador
            if (usuario.Administrator == false)
                return NotFound("O Usuario não é um administrador!");

            filme.IdFilme = _context.Filme.Count() + 1;
            _context.Filme.Add(filme);

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                return NotFound(ex.Message);
            }

            return Ok(filme);

        }

        /// <summary>
        /// Incluir Voto
        /// </summary>
        /// <remarks>
        /// Este método inclui uma avaliação para um determinado filme.
        /// Sample request:
        ///
        ///     PUT / Voto
        ///     {
        ///        "nomeUsuario": "Renato",
        ///        "senha": "renato1234",
        ///        "ativo": true
        ///     }
        ///
        /// </remarks>
        /// <returns>Este método retorna o identiicador do voto foi criado</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpPost("Voto/{NotaAvaliacao},{idUsuario},{idFilme}"), Authorize]
        public ActionResult IncluirVoto(int NotaAvaliacao, int idUsuario, int idFilme)
        {
            //Valida Nota de avaliação
            if (NotaAvaliacao > 4 && NotaAvaliacao < 0)
                return NotFound("A nota de avaliação tem que ser entre 0 e 4");

            var usuario = _context.Usuario.Find(idUsuario);

            //Valida se o usuário existe
            if (usuario == null)
                return NotFound("O Usuario com o id informado não foi encontrado!");

            var filme = _context.Filme.Find(idFilme);

            //Valida se o filme existe
            if (filme == null)
                return NotFound("O Filme com o id informado não foi encontrado!");

            //Valida se o usuário é administrador
            if (usuario.Administrator == true)
                return NotFound("usuarios administradores não podem votar!");


            int IdVoto = _context.Voto.Count() + 1;

            Voto voto = new Voto();

            voto.IdVoto = IdVoto;
            voto.Avaliacao = NotaAvaliacao;
            voto.FilmeID = idFilme;
            voto.UsuarioID = idUsuario;

            _context.Voto.Add(voto);

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                return NotFound(ex.Message);
            }

            return Ok(voto);

        }

        private bool FilmeExists(int id)
        {
            return _context.Filme.Any(e => e.IdFilme == id);
        }
    }
}
