﻿using ApiGerenciamentoIMDB.Services;
using ApiGerenciamentoIMDB.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGerenciamentoIMDB.Controllers
{
    [Route("api/conta")]
    public class AutenticacaoController : ControllerBase
    {

        private readonly BaseContext _context;

        public AutenticacaoController(BaseContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gerar token
        /// </summary>
        /// <remarks>
        /// Este método gera um token que é usado para consumir alguns serviços restritos.
        /// Sample request:
        ///
        ///     POST / Conta
        ///     {
        ///      "nomeUsuario": "Teste",
        ///      "senha": "teste12345"
        ///     }
        ///
        /// </remarks>
        /// <returns>Este método retorna o token para voce inserir no cabeçalho das suas requisições: Bearer + TokenGerado</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public  ActionResult<dynamic> AutenticaUsuario([FromBody]Usuario model)
        {
            var user = _context.Usuario.Where(x => x.NomeUsuario == model.NomeUsuario && x.Senha == model.Senha);

            if (user.Count() > 0)
            {

                var usuarioListado = user.First();                    

                Models.Usuario usuario = new Models.Usuario();

                usuario.NomeUsuario = usuarioListado.NomeUsuario;
                usuario.Senha = usuarioListado.Senha;

                var token = TokenService.GenerateToken(usuario);

                usuario.Senha = "";

                return new
                {
                    Usuario = usuario,
                    token = token
                };
            }
            else
                return NotFound(new { message = "Usuário ou senha inválidos!" });

        }
    }
}
