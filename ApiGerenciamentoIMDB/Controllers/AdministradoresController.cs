﻿using ApiGerenciamentoIMDB.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGerenciamentoIMDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdministradoresController : ControllerBase
    {
        private readonly BaseContext _context;

        public AdministradoresController(BaseContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Incluir Administrador
        /// </summary>
        /// <remarks>
        /// Este método inclui um usuário administrador.
        /// Sample request:
        ///
        ///     POST / Administradores
        ///     {
        ///        "nomeUsuario": "Teste",
        ///        "senha": "teste12345",
        ///        "ativo": true
        ///     }
        ///
        /// </remarks>
        /// <returns>Este método retorna o id do administrador cadastrado</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpPost]
        public async Task<ActionResult<Usuario>> IncluirAdministrador(Usuario usuario)
        {
            usuario.IdUsuario = _context.Usuario.Count() + 1;
            usuario.Administrator = true;

            _context.Usuario.Add(usuario);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UsuarioExists(usuario.IdUsuario))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return usuario;
        }

        /// <summary>
        /// Alterar Administrador
        /// </summary>
        /// <remarks>
        /// Este método altera um usuário administrador.
        /// Sample request:
        ///
        ///     PUT / Administradores
        ///     {
        ///        "nomeUsuario": "Teste",
        ///        "senha": "teste12345",
        ///        "ativo": true
        ///     }
        ///
        /// </remarks>
        /// <returns>Este método retorna o id do administrador alterado</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpPut("{id}"), Authorize]
        public async Task<IActionResult> AlterarAdministrador(int id, Usuario usuario)
        {
            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Apagar Usuário Administrador
        /// </summary>
        /// <remarks>
        /// Este método apaga um usuário administrador
        /// </remarks>
        /// <returns>Este método retorna um indicador indicando se foi apagado corretamente</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpDelete, Authorize]
        public ActionResult ApagarAdministrador(int id)
        {
            var usuario = _context.Usuario.Find(id);

            //Valida se o usuário existe
            if (usuario == null)
                return NotFound("O Usuario com o id informado não foi encontrado!");

            usuario.Ativo = false;
            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex.Message);
            }

            return Ok();
        }


        /// <summary>
        /// Listar Usuários Ativos
        /// </summary>
        /// <remarks>
        /// Este método Lista todos os usuários ativos cadastrados
        /// - Este método permite paginação de dados
        /// </remarks>
        /// <returns>Este método retorna os usuários ativos cadastrados</returns>
        /// <response code="400">Retorno caso ocorra algum erro</response> 
        [HttpGet]
        public ActionResult ListarUsuariosAtivos(int pagina, int quantidadeRegistros)

        {
            var usuarios = _context.Usuario;

            //Caso queira paginar
            if (pagina > 0)
            {
                int totalPaginas = (int)Math.Ceiling(_context.Usuario.Count() / Convert.ToDecimal(quantidadeRegistros));

                var retorno = usuarios.Where(u => u.Ativo == true && u.Administrator == false)
                    .OrderBy(u => u.NomeUsuario)
                    .Skip(quantidadeRegistros * (pagina - 1))
                    .Take(quantidadeRegistros);

                return Ok(retorno.ToList());
            }

            return Ok(usuarios.ToList());
        }

        private bool UsuarioExists(int id)
        {
            return _context.Usuario.Any(e => e.IdUsuario == id);
        }
    }
}
