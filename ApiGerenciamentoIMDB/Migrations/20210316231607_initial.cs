﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiGerenciamentoIMDB.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Filme",
                columns: table => new
                {
                    IdFilme = table.Column<int>(nullable: false),
                    NomeFilme = table.Column<string>(type: "varchar(150)", nullable: false),
                    Diretor = table.Column<string>(type: "varchar(50)", nullable: false),
                    Genero = table.Column<string>(type: "varchar(20)", nullable: false),
                    Atores = table.Column<string>(type: "varchar(500)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Filme", x => x.IdFilme);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    IdUsuario = table.Column<int>(nullable: false),
                    NomeUsuario = table.Column<string>(type: "varchar(50)", nullable: false),
                    Senha = table.Column<string>(type: "varchar(20)", nullable: false),
                    Administrator = table.Column<bool>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.IdUsuario);
                });

            migrationBuilder.CreateTable(
                name: "Voto",
                columns: table => new
                {
                    IdVoto = table.Column<int>(nullable: false),
                    UsuarioID = table.Column<int>(nullable: false),
                    FilmeID = table.Column<int>(nullable: false),
                    Avaliacao = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Voto", x => x.IdVoto);
                    table.ForeignKey(
                        name: "FK_Voto_Filme_FilmeID",
                        column: x => x.FilmeID,
                        principalTable: "Filme",
                        principalColumn: "IdFilme",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Voto_Usuario_UsuarioID",
                        column: x => x.UsuarioID,
                        principalTable: "Usuario",
                        principalColumn: "IdUsuario",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Voto_FilmeID",
                table: "Voto",
                column: "FilmeID");

            migrationBuilder.CreateIndex(
                name: "IX_Voto_UsuarioID",
                table: "Voto",
                column: "UsuarioID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Voto");

            migrationBuilder.DropTable(
                name: "Filme");

            migrationBuilder.DropTable(
                name: "Usuario");
        }
    }
}
